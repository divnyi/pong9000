Rect = Class{}

function Rect:init(point, d, width, height)
  self.point = point
  self.d = d
  self.width = width
  self.height = height
end

function Rect:copy()
  return Rect(self.point:copy(), self.d:copy(), self.width, self.height)
end

function Rect:mirrorOnScreenX()
  self.point:mirrorOnScreenX()
  self.point.x = self.point.x - self.width
  return self
end

function Rect:mirrorOnScreenY()
  self.point:mirrorOnScreenY()
  self.point.y = self.point.y - self.height
  return self
end

function Rect:mirrorOnScreen()
  return self:mirrorOnScreenX():mirrorOnScreenY()
end

topBound, bottomBound, leftBound, rightBound = 1, 2, 4, 8

function Rect:clipToBounds()
  local result = 0
  if self.point.x < 0 then
    self.point.x = 0
    result = result + leftBound
  end
  if self.point.x + self.width > VIRTUAL_WIDTH then
    self.point.x = VIRTUAL_WIDTH - self.width
    result = result + rightBound
  end
  
  if self.point.y < 0 then
    self.point.y = 0
    result = result + topBound
  end
  if self.point.y + self.height > VIRTUAL_HEIGHT then
    self.point.y = VIRTUAL_HEIGHT - self.height
    result = result + bottomBound
  end
  
  return result
end

function Rect:collides(rect)
  if self.point.x+self.width < rect.point.x then
    return false
  end
  if rect.point.x+rect.width < self.point.x then
    return false
  end

  if self.point.y+self.height < rect.point.y then
    return false
  end
  if rect.point.y+rect.height < self.point.y then
    return false
  end

  return true
end

function Rect:update(dt)
  self.point:add(self.d:copy():multiply(dt))
end
                          
function Rect:render()
  love.graphics.rectangle('fill', self.point.x, self.point.y, self.width, self.height)
end
