Game = Class{}

gameLoaded, gameStarted, gameFinished = 1, 2, 3

function Game:init()
  self.player1 = Player(
    constants.firstPaddleStartingRect:copy(),
    constants.player1.up,
    constants.player1.down,
    constants.color.paddle1,
    constants.color.ball1,
    1
  )
  self.player2 = Player(
    constants.firstPaddleStartingRect:copy():mirrorOnScreen(),
    constants.player2.up,
    constants.player2.down,
    constants.color.paddle2,
    constants.color.ball2,
      -1
  )

  self.balls = {}
  self.state = gameLoaded
  self.fps = 60

  self.volume = 100
end

function Game:update(dt)
  self.player1:update(dt)
  self.player2:update(dt)

  self.fps = love.timer.getFPS()

  if self.state == gameStarted then
    self.time = self.time + dt
    for idx, ball in ipairs(self.balls) do
      local result = ball:update(dt)

      if ball.player == self.player2 then
        self:collision(ball, self.player1)
      else
        self:collision(ball, self.player2)
      end

      if result == rightBound then
        self:score(ball, self.player1)
      elseif result == leftBound then
        self:score(ball, self.player2)
      end
    end

    self:volumeUpdate()
    
    if self.player1.score > constants.scoreLimit
    or self.player2.score > constants.scoreLimit then
      love.audio.stop()
      constants.sound.win:play()
      self.state = gameFinished
    end
  end
end


function Game:score(ball, player)
  self:increaseScore(player)
  self.ballsScored = self.ballsScored + 1

  ball.rect.point:mirrorOnScreenX()
  ball.rect:clipToBounds()
  
  self:cloneBall(ball, player)
end

function Game:collision(ball, player)
  if player.rect:collides(ball.rect) then
    constants.sound.deflect:play()
    ball:didCollide(player)
    self:cloneBall(ball, player)
    if self.ballCount > constants.deflectedBallsTrippleAfter then
      self:cloneBall(ball, player)
    end
    if self.ballCount > constants.deflectedBallsQuadAfter then
      self:cloneBall(ball, player)
    end
    ball:speedUp()
  end
end

function Game:cloneBall(ball, player)
  if self.fps >= constants.fpsLowerBound then
    table.insert(self.balls, ball:cloneAndFizzle())
  else
    self:increaseScore(player)
  end
end

function Game:increaseScore(player)
  player.score = player.score + self:scoreMod()
end

function Game:scoreMod()
  local restScored = self.ballsScored
  for idx, stopsCount in ipairs(constants.scoreMods.stops) do
    restScored = restScored - stopsCount
    if restScored < 0 then
      local score = constants.scoreMods.values[idx]
      self.restScored = -restScored
      return score
    end
  end
  return 1
end

function Game:volumeUpdate()
  local count = self.ballCount
  local borderVolume = self:vol(4.0/math.pow(count, 0.6))*constants.borderVolumeMult
  constants.sound.border:setVolume(borderVolume)
  local scoreVolume = self:vol(2.0/math.pow(count, 0.5))
  constants.sound.score:setVolume(scoreVolume)
  local deflectVolume = self:vol(1.0/math.pow(count, 0.2))
  constants.sound.deflect:setVolume(deflectVolume)
  local winVolume = self:vol(1.0)*constants.winVolumeMult
  constants.sound.win:setVolume(winVolume)
end

function Game:vol(double)
  return math.min(1.0, double) * (self.volume/100.0)
end

function Game:volumeDown()
  self.volume = math.max(0.0, self.volume - 10)
end

function Game:volumeUp()
  self.volume = math.min(100, self.volume + 10)
end

function Game:restartGame()
  self.player1.rect = constants.firstPaddleStartingRect:copy()
  self.player2.rect = constants.firstPaddleStartingRect:copy():mirrorOnScreen()
  
  self.state = gameStarted

  self.time = 0

  self.player1.score = 0
  self.player2.score = 0
  self.ballsScored = 0
  self.restScored = 0
  
  self.ballCount = 0
  self.balls = {}
  table.insert(self.balls, Ball.playerBall(self.player1))
  table.insert(self.balls, Ball.playerBall(self.player2))

  self.ballsDependable = {
    ballAlpha = 1.0
  }
end

function Game:keypressed(key)
  if key == "escape" then
    love.event.quit()
  elseif key == "enter" or key == "return" then
    self:restartGame()
  end

  if key == "6" then
    self:volumeDown()
  elseif key == "7" then
    self:volumeUp()
  end
end

function Game:draw()
  self.ballCount = table.getn(self.balls)
  for idx, ball in ipairs(self.balls) do
    ball:render()
  end

  self.player1:render()
  self.player2:render()
end
