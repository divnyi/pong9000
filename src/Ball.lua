Ball = Class{}

function Ball:init(player, rect)
  self.player = player
  self.rect = rect
  
  return self
end

function Ball.playerBall(player)
  local rect = player.rect:copy()
  rect.point.x = rect.point.x + math.random(0, rect.width)
  rect.point.y = rect.point.y + math.random(0, rect.height)
  rect.width = constants.ballSize
  rect.height = constants.ballSize

  local randomAngle = math.random(constants.minAngle, constants.maxAngle)
  local randomLen = math.random(constants.speedMin, constants.speedMax)
  rect.d = Coord.fromRadial({ len = randomLen, angle = randomAngle })
  rect.d.x = player.direction * math.abs(rect.d.x)
  if math.random(0, 1) == 1 then
    rect.d.y = -rect.d.y
  end

  return Ball(player, rect)
end

function Ball:cloneAndFizzle()
  local rect = self.rect:copy()

  local minAngle = constants.maxAngle
    - (constants.maxAngle-constants.minAngle)
    * math.min(1.0, 8.0/math.sqrt(game.ballCount))
  local maxAngle = constants.maxAngle
  local randomAngle = math.random(math.random(minAngle, maxAngle), maxAngle)
  local randomLen = math.random(constants.speedMin, constants.speedMax)
  rect.d = Coord.fromRadial({ len = randomLen, angle = randomAngle })

  rect.d.x = self.player.direction * math.abs(rect.d.x)
  rect.d.y = self.rect.d.y<0 and -rect.d.y or rect.d.y
  
  return Ball(self.player, rect)
end

function Ball:didCollide(player)
  self.player = player

  self.rect.d.x = player.direction * math.abs(self.rect.d.x)
end

function Ball:speedUp()
  local radial = self.rect.d:toRadial()
  radial.len = math.min(radial.len + constants.returnSpeedInc, constants.returnSpeedMax)
  self.rect.d = Coord.fromRadial(radial)
end

function Ball:update(dt)
  self.rect:update(dt)
  local bounds = self.rect:clipToBounds()
  if bit.band(bounds, topBound) ~= 0 then
    constants.sound.border:play()
    self.rect.d.y = math.abs(self.rect.d.y)
  elseif bit.band(bounds, bottomBound) ~= 0 then
    constants.sound.border:play()
    self.rect.d.y = -math.abs(self.rect.d.y)
  elseif bit.band(bounds, leftBound) ~= 0 then
    constants.sound.score:play()
    return leftBound
  elseif bit.band(bounds, rightBound) ~= 0 then
    constants.sound.score:play()
    return rightBound
  end
  return 0
end

function Ball:render()
  local ballAlpha = 15.0/math.sqrt(game.ballCount)+0.05, 1.0
  local extendedWidth = constants.sideAlphaBase*VIRTUAL_WIDTH
  local sideAlphaMod
  if self.player.direction < 0 then
    sideAlphaMod = (extendedWidth + VIRTUAL_WIDTH - self.rect.point.x) / VIRTUAL_WIDTH
  else
    sideAlphaMod = (extendedWidth + self.rect.point.x) / VIRTUAL_WIDTH
  end
  self.player.ballColor:set(ballAlpha * sideAlphaMod)
  
  self.rect:render()
end
