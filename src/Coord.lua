Coord = Class{}

function Coord:init(x, y)
  self.x = x
  self.y = y
end

function Coord:copy()
  return Coord(self.x, self.y)
end

function Coord:mirrorOnScreenX()
  self.x = VIRTUAL_WIDTH - self.x
  return self
end

function Coord:mirrorOnScreenY()
  self.y = VIRTUAL_HEIGHT - self.y
  return self
end

function Coord:add(arg)
  self.x = self.x + arg.x
  self.y = self.y + arg.y
  return self
end
                     
function Coord:multiply(arg)
  self.x = self.x * arg
  self.y = self.y * arg
  return self
end

function Coord.fromRadial(radial)
  local angle = (radial.angle+3600) % 360
  local x = math.cos(math.pi * angle / 180) * radial.len
  local y = math.sin(math.pi * angle / 180) * radial.len
  return Coord(x, y)
end

function Coord:toRadial()
  local len = math.sqrt(self.x*self.x+self.y*self.y)
  local radians = math.atan2(self.y, self.x)
  local angle = (radians)/math.pi*180
  return { len = len, angle = angle }
end
