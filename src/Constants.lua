Constants = Class{}

VIRTUAL_WIDTH = 320
VIRTUAL_HEIGHT = 200

WINDOW_WIDTH = VIRTUAL_WIDTH*4
WINDOW_HEIGHT = VIRTUAL_HEIGHT*4

require 'Color'

function Constants:init()
  self.scoreLimit = 9000
  
  self.paddleSpeed = 200

  self.firstPaddleStartingRect = Rect(Coord(10, 30), Coord(0, 0), 5, 40)
  self.ballSize = 5

  self.speedMin = 80
  self.speedMax = 100
  self.returnSpeedInc = 7
  self.returnSpeedMax = 130

  self.minAngle = 35
  self.maxAngle = 68

  self.fizzleAngleMin = 5
  self.fizzleAngleMax = 10

  self.scoreBallsDoublesUntil = 50
  self.deflectedBallsTrippleAfter = 1400
  self.deflectedBallsQuadAfter = 7000
  self.fpsLowerBound = 58

  self.sideAlphaBase = 0.1

  self.scoreMods = {
    stops = { 1, 20, 100, 200, 500 },
    values = { 500, 100, 20, 5, 2 }
  }

  self.color = {
    paddle1 = Color(0.8, 0.4, 0.4, 1),
    paddle2 = Color(0.4, 0.4, 0.8, 1),
    ball1 = Color(0.9, 0.4, 0.5, 1),
    ball2 = Color(0.45, 0.4, 0.8, 1),
    fps = Color(0, 1, 0, 0.5),
    text = Color(1, 1, 1, 1),
    score = Color(1, 1, 1, 0.3)
  }

  self.font = {
    small = love.graphics.newFont('font.ttf', 8),
    large = love.graphics.newFont('font.ttf', 16),
    score = love.graphics.newFont('font.ttf', 32)
  }

  self.sound = {
    score = love.audio.newSource('sounds/score.wav', 'static'),
    border = love.audio.newSource('sounds/border.wav', 'static'),
    deflect = love.audio.newSource('sounds/deflect.wav', 'static'),
    win = love.audio.newSource('sounds/win.wav', 'static')
  }

  self.player1 = {
    up = 'w',
    down = 's'
  }

  self.player2 = {
    up = 'up',
    down = 'down'
  }

  self.borderVolumeMult = 0.8
  self.winVolumeMult = 1.5
end
