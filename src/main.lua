-- https://github.com/Ulydev/push
push = require 'push'

-- https://github.com/vrld/slam
slam = require 'slam'

-- https://github.com/vrld/hump/blob/master/class.lua
Class = require 'class'

require 'Coord'
require 'Rect'

require 'Constants'

require 'Player'
require 'Ball'
require 'Game'
require 'UI'

function love.load()
    love.graphics.setDefaultFilter('nearest', 'nearest')

    math.randomseed(os.time())

    constants = Constants()

    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
                       fullscreen = false,
                       resizable = true,
                       vsync = true,
                       canvas = false
    })

    game = Game()
    ui = UI()
end

function love.resize(w, h)
  push:resize(w, h)
end

function love.update(dt)
  game:update(dt)
  ui:update(dt)
end

function love.keypressed(key)
  game:keypressed(key)
end

function love.draw()
  push:start()

  love.graphics.clear(40/255.0, 45/255.0, 52/255.0, 1)

  game:draw()
  ui:draw()

  push:finish()
end
