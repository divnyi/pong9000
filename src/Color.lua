Color = Class{}

function Color:init(r, g, b, a)
  self.r = r
  self.g = g
  self.b = b
  self.a = a
end

function Color:set(alpha)
  love.graphics.setColor(self.r, self.g, self.b, alpha or self.a)
end
