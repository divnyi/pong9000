UI = Class{}

function UI:init()
  love.window.setTitle('Pong 9000')
end

function UI:update(dt)
end

function UI:draw()
  if game.state == gameLoaded then
    self:welcomeMessage()
  else
    self:scores()
  end
  if game.state == gameFinished then
    self:endgameMessage()
  end
  
  self:displayFPS()
  self:displayBallsCount()
  self:displayVolume()
end

function UI:welcomeMessage()
  love.graphics.setFont(constants.font.small)
  constants.color.text:set()

  love.graphics.printf('Welcome to Pong 9000', 0, 10, VIRTUAL_WIDTH, 'center')
  love.graphics.printf('Press Enter to begin!', 0, 20, VIRTUAL_WIDTH, 'center')
  love.graphics.printf('Player1 controls: W S keys', 0, 40, VIRTUAL_WIDTH, 'center')
  love.graphics.printf('Player2 controls: up/down', 0, 50, VIRTUAL_WIDTH, 'center')
  love.graphics.printf('Volume controls: down - 6, up - 7', 0, 65, VIRTUAL_WIDTH, 'center')
end

function UI:endgameMessage()
  local whoWins = ""
  if game.player1.score > game.player2.score then
    whoWins = "Player1 wins!"
  elseif game.player1.score < game.player2.score then
    whoWins = "Player2 wins!"
  else
    whoWins = "Nobody win :("
  end
  constants.color.text:set()
  love.graphics.setFont(constants.font.large)
  love.graphics.printf("IT'S OVER NINE THOUSAND!",
                       0, 10, VIRTUAL_WIDTH, 'center')
  love.graphics.setFont(constants.font.small)
  love.graphics.printf(whoWins, 0, 30, VIRTUAL_WIDTH, 'center')
  love.graphics.printf('Press Enter to restart!', 0, 50, VIRTUAL_WIDTH, 'center')
end

function UI:scores()
  if game.state == gameFinished then
    constants.color.text:set()
  else
    constants.color.score:set()
  end
  love.graphics.setFont(constants.font.large)

  local scoreMod = game:scoreMod()
  if scoreMod ~= 1 then
    if scoreMod == constants.scoreMods.values[1] then
      love.graphics.printf(
        string.format("First blood at x%s", game:scoreMod()),
        0,
        VIRTUAL_HEIGHT / 3 - 20,
        VIRTUAL_WIDTH,
        'center'
      )
    else
      love.graphics.printf(
        string.format("Next %s - x%s", game.restScored, game:scoreMod()),
        0,
        VIRTUAL_HEIGHT / 3 - 20,
        VIRTUAL_WIDTH,
        'center'
      )
    end
  end

  love.graphics.setFont(constants.font.score)

  love.graphics.printf(
    tostring(game.player1.score),
    0,
    VIRTUAL_HEIGHT / 3,
    VIRTUAL_WIDTH / 2 - 30,
    'right'
  )
  love.graphics.printf(
    "-",
    VIRTUAL_WIDTH / 2 - 20,
    VIRTUAL_HEIGHT / 3,
    40,
    'center'
  )
  love.graphics.printf(
    tostring(game.player2.score),
    VIRTUAL_WIDTH / 2 + 30,
    VIRTUAL_HEIGHT / 3,
    VIRTUAL_WIDTH / 2,
    'left'
  )
end

function UI:displayFPS()
  love.graphics.setFont(constants.font.small)
  constants.color.fps:set()
  love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()), 5, 5)
end

function UI:displayBallsCount()
  love.graphics.setFont(constants.font.small)
  constants.color.fps:set()
  love.graphics.print('Balls: ' .. tostring(table.getn(game.balls)), 5, 12)
end

function UI:displayVolume()
  love.graphics.setFont(constants.font.small)
  constants.color.fps:set()
  love.graphics.printf(
    'Volume: ' .. tostring(game.volume) .. "%",
    5, 5,
    VIRTUAL_WIDTH - 10,
    'right'
  )
end
