Player = Class{}

function Player:init(rect, upKey, downKey, color, ballColor, direction)
  self.rect = rect
  self.upKey = upKey
  self.downKey = downKey
  self.color = color
  self.ballColor = ballColor
  self.direction = direction
  self.score = 0
end

function Player:update(dt)
  if game.state == gameFinished then
    self.rect.d.y = 0
  else
    if love.keyboard.isDown(self.upKey) then
      self.rect.d.y = -constants.paddleSpeed
    elseif love.keyboard.isDown(self.downKey) then
      self.rect.d.y = constants.paddleSpeed
    else
      self.rect.d.y = 0
    end
  end
  
  self.rect:update(dt)
  self.rect:clipToBounds()
end

function Player:render()
  self.color:set()
  self.rect:render()
end
